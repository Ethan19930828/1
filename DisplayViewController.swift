//
//  DisplayViewController.swift
//  addressBookTest2
//
//  Created by zwx on 30/11/2014.
//  Copyright (c) 2014 zwx. All rights reserved.
//

import UIKit

class DisplayViewController: UIViewController {

    //@IBOutlet weak var nameDetails: UITextView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var nameText:String?
    var numberText:String?
    var emailText:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let nameInfo:String = nameText {
            self.nameLabel.text = nameInfo
        }
        
        if let numberInfo:String = numberText{
            self.numberLabel.text = numberInfo
        }
        
        if let emailInfo:String = emailText {
            self.emailLabel.text = emailInfo
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
