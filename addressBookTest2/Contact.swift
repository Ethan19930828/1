//
//  Contact.swift
//  addressBookTest2
//
//  Created by Jay Kim on 01/12/2014.
//  Copyright (c) 2014 Jay Kim. All rights reserved.
//

import Foundation
import CoreData

class Contact: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var number: String

}
