//
//  AddViewController.swift
//  addressBookTest2
//
//  Created by zwx on 30/11/2014.
//  Copyright (c) 2014 zwx. All rights reserved.
//

import UIKit

class AddViewController: UIViewController, UIGestureRecognizerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet var swipeDown: UISwipeGestureRecognizer!
    
    @IBAction func swipe(sender: UISwipeGestureRecognizer) {
        println("swipe")
        self.nameField.resignFirstResponder()
        self.numberField.resignFirstResponder()
        self.emailField.resignFirstResponder()
    }
    
    
    @IBAction func closeView(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(self.swipeDown)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
