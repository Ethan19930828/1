//
//  ListViewController.swift
//  addressBookTest2
//
//  Created by zwx on 30/11/2014.
//  Copyright (c) 2014 zwx. All rights reserved.
//

import UIKit

struct Contact {
    var name:String
    var number:String
    var email:String
}

class ListViewController: UITableViewController {
    
    //var savedItem:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    
    private var contacts = Array<Contact>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    
    @IBAction func unwindToList(segue: UIStoryboardSegue) {
        println("segue unwound")
        
        let contact = segue.sourceViewController as AddViewController
        let name = contact.nameField.text
        let number = contact.numberField.text
        let email = contact.emailField.text
        
        var newContact:Contact = Contact(name: name, number: number, email: email)
        self.contacts.append(newContact)
        println("currently \(self.contacts.count) indexes")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.contacts.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("contacts", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...
        let contact:Contact = self.contacts[indexPath.row]
        cell.textLabel?.text = contact.name
        cell.detailTextLabel?.text = contact.number
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            self.contacts.removeAtIndex(indexPath.row)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showContacts" {
            let destination = segue.destinationViewController as DisplayViewController
            
            let indexPath = self.tableView.indexPathForSelectedRow()
            if let row:Int = indexPath?.row {
                let contact:Contact = contacts[row]
                destination.nameText = contact.name
                destination.numberText = contact.number
                destination.emailText = contact.email
            }
        }
    }
}
